# Participants need

- A laptop
  - *Must* run some kind of Linux distribution, or a VM
- An internet connection (codethink guest?)
  - We'll need to have this connected to the artifact server

# Prep

We need:
- Filled artifact server
- Ready-to-clone project
  - Should contain project.conf and a base element
  - Should be linked up to the artifact server
  - Other parts will be written manually during the intro

# Intro

- Want to get people up to speed on working with BuildStream
- We'll go from BuildStream installation to developing on a project
- Most of this is covered in the BuildStream documentation, too
- Expected to be very hands-on

- Tomorrow (I believe) there will be a 102 session that goes through
  some more advanced topics mostly related to systems integration.

# But first... What *is* BuildStream

- Actually pretty hard to nail this definition
  - If you check ML/IRC discussions from about half a year ago or so
    you can see Paul and Tristan argue over this

- Eventually we arrived at "an integration tool"
  - i.e., like a build tool (make), but instead of actually running a
    compiler it runs other build tools (make), and puts their results
    together
  - So you'll have your definition for bash, one for emacs, another
    for vim, and BuildStream will take those definitions and build a
    working system that can run all three.
  - YOCTO/Baserock/distro people probably understand what's going on
    here

- BuildStream is also a YAML-based language to define build metadata
  - What you would expect - you can write a graph of definitions that
    can successfully build, for example, a Linux distribution. We also
    - Allow nesting "projects" (i.e., if you want to have something
      like the freedesktop-sdk that forms the base of larger systems)
    - Allow building with multiple targets that are distributed in
      different ways (e.g., flatpak + pip + docker + AUR package)

- BuildStream *also* contains a number of features to simplify
  developing on these software stacks
  - Sandboxed environment
  - No host tools
  - QOL features for development
  - Distributed builds/artifacts

- But really, the best way to understand it is to give it a try - any
  questions so far? If not, let's jump right in!

# Installation

We will need some distraction for fast finishers, probably a slide
that lists basic commands and asks them to play around for a bit.

- Raise hands for distros in the room
  - Shout out Arch, Fedora, Ubuntu LTS >=18.04 (Bionic Beaver) or Debian
  - Then, ask who hasn't raised their hands
    - Mention Ubuntu LTS 16.04 (Xenial Xerus)
    - Ask the one or two people what they're using
      - Improvise!

- Arch/Fedora users
  - There are unofficial packages for your distros!
  - You'll need to install from the AUR or a copr
    - Arch users, install `buildstream` using your favorite AUR
      manager, e.g. `aurman -S buildstream`
      - If you don't have an AUR manager, just do what the other
        distro people do
    - Fedora users, enable the `bochecha/buildstream` copr (`sudo dnf
      copr enable bochecha/buildstream`) and install BuildStream
      (`sudo dnf install buildstream`)

- Other
  - We'll go through installing from pip
  - First, ask people to install python, pip, fuse, bubblewrap, tar and git
    - Internet might be slow? ~8 * ~500 MB, so about a gig or four.
  - Show distro-specific commands on slide

- For anything particularly exotic, get them to use docker
  - I can play personal support for a bit

# Let's look at a project

- First, if you want to follow along, cloning our project will
  probably be handy
  - This might be part of the final slide for the installation bit,
    though
  - If you did want to start from scratch, you would create a new
    directory and run `bst init` inside it

- We'll go through the files currently in the project

  - project.conf (required)
    - name - just a name for the project (required)
    - format-version - the BuildStream format version; used to signify
                       version compatibility (optional)
    - element-path - a relative directory to store elements in; for
                     neatness, we'll cover what this is in a minute
                     (optional)
    - aliases - we'll get to these in a bit, but essentially just URLs
                that we use frequently (optional)
    - artifact-server - we'll explain this thing later, but copy it
                        please if you're manually following along
  - README.md - a readme file, duh (optional)
  - elements (order is deliberate)
    - alpine/base.bst
      Contains "alpine linux", a teeny-tiny linux distribution that
      uses busybox instead of a full gnu toolchain.

      - kind - the element "kind". This tells BuildStream what to do
             with this element. In this case, we'll simply take our
             input and put it in our system (required)
      - description - just a description (optional)
      - sources - the source files for this element, in this case, a
                  tarball that contains alpine linux (optional)
        - kind - the source "kind". This tells BuildStream how to get
                 the files (i.e., tar, git, bzr, ostree, ...).
        - url - a link to the source
        - ref - a unique identifier for the source, to check data
                integrity and ensure version matches

    - base.bst

      This is by convention (not actually enforced by BuildStream)
      what we call the "base system".

      Anyone here probably knows this already, but BuildStream will
      build projects independent from the host system. It still needs
      a basic toolchain to do anything though, and this is where
      `base.bst` comes in.

      - stack elements Two new keywords here - "stack" and
        "depends".

        "Depends" is fairly self-explanatory, it simply means "when we
        do something with this element, we need this other element"
        (BuildStream does distinguish between run- and build-
        dependencies, but we'll ignore that complexity for now).

        "stack" elements are elements that simply take a number of
        dependencies, and put them in the same place. This way, for
        example, if we wanted an element with both, say, firefox and
        vim, but not have one depend on the other, we can combine them
        this way.

      In this case, we just want to grab `base/alpine.bst`, but more
      complex projects might bootstrap a whole toolchain
      element-by-element (freedesktop-sdk does this, for example). We
      therefore by convention make `base.bst` a `stack` element that
      pulls together all components of the base image.

# Enough gawking, let's build something!

- Call `bst build base.bst`
- Btw, this won't take too long because you're downloading it straight
  from our artifact server!

- So, alright, what happened...
  - BuildStream went and looked at our .bst files, and figured out
    that it needs to first build alpine.bst and then base.bst because
    of our `depends` value.
  - It then went to the server we specified in our alies, downloaded
    the alpine tarfile, and placed that in an "artifact", i.e., a
    ready-built object hoarded for you by BuildStream.
  - Then we registered that this tarfile is part of the base.bst
    stack.

- Let's check what's in our artifact
  - `bst checkout base.bst base`
  - `ls base`
  - Aha, a typical Linux root directory (you'll find that this is what
    alpine Linux looks like)!

# What now?

- Let's build an actual application
  - We'll build a typical hello world program, the one from the
    automake documentation

- We modify `project.conf` to include a `gnu:` alias
- We then create `hello.bst`:
  - kind: autotools - this will essentially run `autoreconf;
                      ./configure; make; make install` for us
  - variables - a set of "variables" used to configure what exactly
                BuildStream does
    - command-subdir - in this case, we ask BuildStream to run the
                       autotools tools in a subdirectory of the
                       project, specifically the doc/amhello
                       directory, which contains the source code for
                       automake's hello world example

    - Variables are resolved in the following order:
      1. BuildStream defaults
      2. `project.conf` settings
      3. element `kind` defaults
      4. element declarations (yaml files)

      Defaults include, for example, `%{prefix}`, which defines the
      default installation location. By default, this is set to
      `/usr` - try setting it to something else later, and see how
      this affects `hello.bst`

  - sources - as before
  - depends - as before

- We can again look at the contents
  - `bst checkout hello.bst hello`
  - `ls hello`
  - This time, we managed to check out the base runtime *as well as* the hello artifact
    - A file called `usr/bin/hello` should exist in this checkout
  - Let's try to run `./hello/usr/bin/hello`
    - We get 'no such file or directory', because our machine won't
      know where to look for linked libraries
    - So how do we debug this then?

- Let's try `bst shell hello.bst`!
  - This will place us inside our base runtime, with the artifact from
    `hello.bst` staged inside it
    - This means that whatever we built in `hello.bst` is now in our
      runtime, and we can use it as if we were inside that runtime.
  - Let's run `/usr/bin/hello`
    - It works!

# Let's greet in a different language

- We'll need to patch the actual project to do this.
  - Luckily, BuildStream makes this somewhat simple.

- Run `bst workspace open hello.bst hello-src`
- Let's modify `hello-src/doc/amhello/src/main.c`

- Now, let's build `hello.bst` again
  - BuildStream will show that it's using a "workspace"

- Let's try running `hello` in our shell
  - We have a new message!

- Note that workspaces *also* enable what we call incremental build
  - I.e., when make detects that a file has already been compiled, it
    won't compile it again
  - This means that, for an individual project, using BuildStream is
    pretty much like using the normal build tool

# Fin

- This covers pretty much everything you need to know to work on a
  project with BuildStream
- For more details, the documentation explains exactly how plugins
  work, and which are available
- Also visit the 102 session if you want to know more about the
  integration workflow

# For those of you who want to stick around, DOOM awaits

- Let's build DOOM with BuildStream!
